def get_age_difference(cycle):

    len_cycle = len(cycle)
    result_cycle = []
    for i in range(len_cycle):
        age_difference = abs(db[cycle[i % len_cycle]].donor.age - db[cycle[(i + 1) % len_cycle]].patient.age)
        result_cycle.append(age_difference)
    return result_cycle

def normalize(cycle):
    cmin = min(cycle)
    while cycle[0] != cmin:
        v = cycle.pop(0)
        cycle.append(v)


def all_cycles(cycles, path, node, tovisit, adj,K):
    for i in adj[node]:
        if i in path:
            j = path.index(i)
            cycle = path[j:]+[node]
            normalize(cycle)
            cycles.add(tuple(cycle))

        if i in tovisit:
            if K-1 > 0:
                all_cycles(cycles, path+[node], i, tovisit-set([i]), adj, K-1)
    return cycles


def get_all_cycles(adj,K):
    tovisit = set(adj.keys())
    visited = set([])
    cycles = set([])
    for i in tovisit:
        tmpvisit = set(tovisit)
        tmpvisit.remove(i)
        first = i
        all_cycles(cycles, [], first, tmpvisit, adj, K)
    return cycles


EPS = 1.e-4
from gurobipy import *
import networkx as nx
from networkx.algorithms import isomorphism
import time

def kep_cycle(adj,cycles):
    model = Model("KEP cycle formulation")
    model.Params.OutputFlag = 0  # silent mode
    x={}
    cmap, nmap, on_cyc = {}, {}, {}
    for i in adj:
        on_cyc[i] = set()
    n = 1
    for c in cycles:
        cmap[c] = n
        nmap[n] = c
        for i in c:
            on_cyc[i].add(n)
        x[n] = model.addVar(vtype="B", name="x(%s)"%(n))
        n += 1
    model.update()
    print("created variables", time.clock(), "s")

    # Ensure that each pair only appears in one cycle for each solution
    for i in on_cyc:
        cs = on_cyc[i]
        if len(cs) > 1:
            model.addConstr(quicksum(x[j] for j in cs) <= 1, "cycles(%s)" % i)
    # model.addConstr(quicksum(model.getVars()) >= 1, "NoEmptySols")
    print("added constraints", time.clock(), "s")

    recursive = False
    if not recursive:
        all_age_diffs=[get_age_difference(c) for c in cycles]
        all_age_diffs=[max(c) for c in all_age_diffs]
        max_age_diff = max(all_age_diffs)
        # Define the coefficients of the objective function
        first_run = True
        # crits = ['max_transplants', 'min_age_diff']
        crits = ['min_age_diff']
        for (i,crit) in enumerate(crits):
            E = {}
            for c in cycles:
                # make graph for current cycle
                # arcs = []
                # for i in adj:
                #     for j in adj[i]:
                #         if i in c and j in c:
                #             arcs.append((i,j))
                # G = nx.DiGraph()
                # G.add_edges_from(arcs)

                # Get time of entry

                # Get age difference
                if crit == 'max_transplants':
                    E[c] = len(c)
                else:
                    #minimize sum of age diffs
                    # E[c] = sum(get_age_difference(c))

                    # sum PRA
                    E[c] = sum([*map(lambda x: db[x].patient.PRA,c)])

                    # count patients with PRA above 0.80
                    # E[c] = sum([*filter(lambda x: db[x].patient.PRA >0.8,c)])

                    # count number of blood type O patients
                    # E[c] = sum([*filter(lambda x: db[x].patient.ABO == 'O', c)])

            print("calculated obj.coefficients", time.clock(), "s")
            if i == 0:
                model.setObjective(quicksum(E[c] * x[cmap[c]] for c in cycles), GRB.MINIMIZE)
            else:
                model.addConstr(model.getObjective() >= model.PoolObjVal, f"CritConstr({i})" )
                model.setObjective(quicksum(E[c] * x[cmap[c]] for c in cycles), GRB.MAXIMIZE)
            print("updated objective", time.clock(), "s")

            model.Params.PoolSearchMode = 2  # set search mode for finding multiple solutions
            model.Params.PoolSolutions = 1000  # if i < len(all_toe)-1 else 1000  # !!!!! set the number of different solutions desired
            model.Params.PoolGap = 0  # find only solutions 0% worse that optimum
            model.optimize()
            print("number of solutions found:", model.SolCount)

            sol = []
            for j in x:
                if x[j].X > EPS:
                    sol.append(nmap[j])
            print("optimum solution:", sol)

            all_sols = set()
            for i in range(model.SolCount):
                model.Params.SolutionNumber = i
                print(i, "objective:", model.PoolObjVal, end="\t")
                sol = []
                for j in x:
                    if x[j].Xn > EPS:
                        sol.append(nmap[j])
                sol.sort()
                print(sol)
                sol = frozenset(sol)
                assert sol not in all_sols
                all_sols.add(sol)
            print("----------------------------------------------------------------------")

    else:
    # Define the coefficients of the objective function (lexigraphical order version)

        first_run = True
        for i in range(len(all_toe)):

            E = {}
            for c in cycles:
                # make graph for current cycle
                # arcs = []
                # for i in adj:
                #     for j in adj[i]:
                #         if i in c and j in c:
                #             arcs.append((i,j))
                # G = nx.DiGraph()
                # G.add_edges_from(arcs)

                # Get time of entry
                is_max_toe = lambda x: db[x].patient.time_of_entry == all_toe[i]
                filter_toe = [*filter(is_max_toe, c)]

                # Get age difference
                # E[c] = len(filter_toe)

                # Get PRA
                E[c] = sum([*map(lambda x:db[x].patient.PRA)])
            print("calculated obj.coefficients", time.clock(), "s")

            if not first_run:
                model.addConstr(model.getObjective() >= model.PoolObjVal, f"CritConstr({i})" )
            model.setObjective(quicksum(E[c] * x[cmap[c]] for c in cycles), GRB.MAXIMIZE)
            print("updated objective", time.clock(), "s")

            model.Params.PoolSearchMode = 2     # set search mode for finding multiple solutions
            model.Params.PoolSolutions = 1000 #if i < len(all_toe)-1 else 1000  # !!!!! set the number of different solutions desired
            model.Params.PoolGap = 0            # find only solutions 0% worse that optimum
            model.optimize()
            print("number of solutions found:", model.SolCount)
            print(-(i+1)%len(all_toe))
            first_run = False

            sol = []
            for j in x:
                if x[j].X > EPS:
                    sol.append(nmap[j])
            print("optimum solution:", sol)


            all_sols = set()
            for i in range(model.SolCount):
                model.Params.SolutionNumber = i
                print(i, "objective:", model.PoolObjVal, end="\t")
                sol = []
                for j in x:
                    if x[j].Xn > EPS:
                        sol.append(nmap[j])
                sol.sort()
                print(sol)
                sol = frozenset(sol)
                assert sol not in all_sols
                all_sols.add(sol)
            print("----------------------------------------------------------------------")

    return all_sols



if __name__ == "__main__":
    import sys
    try:
        filename = "enumeraodeciclos/in.input.gz"#sys.argv[1]
    except:
        print("usage: %s instance")
        exit(0)
     
    from kep_io import read_kep
    from data_utils import read_charact

    db = read_charact()
    adj, w = read_kep(filename)

    all_toe = [*map(lambda x: x.patient.time_of_entry, db.values())]
    all_toe = sorted(all_toe, reverse=True)
    # all_toe = sorted(all_toe)

    edges = []
    for i in adj:
        for j in adj[i]:
            edges.append((i, j))
    G = nx.DiGraph()
    G.add_edges_from(edges)
    #import matplotlib.pyplot as plt
    #nx.draw_graphviz(G,prog='neato')
    #plt.show()
    #exit(0)
        
    cycles = get_all_cycles(adj,3)
    print(len(cycles), "cycles", time.clock(), "s")
    for c in cycles:
        print("\t", c)
    all_sols = kep_cycle(adj, cycles)
    print("number of different solutions:", len(all_sols))
