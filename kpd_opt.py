import time
from gurobipy import *
from kep_io import read_kep
from misc.kep_cycle_opt_jviana import get_all_cycles

from data_utils import read_charact

class KPD:

    def __init__(self, name=None):
        self.name = name
        self._cycles = None
        self.solutions = None
        self._opt_restrictions = []

    ###
    # DATA METHODS
    ###
    def load_pair_data(self, data):
        if isinstance(data, str):
            self._db = read_charact(data)
            self._instance_updated = False

    def load_cycles(self, filename, cycle_length=3):
        self._create_cycles(filename, cycle_length)

    def _create_cycles(self, filename, length=3):
        # TODO implement method to create input graph
        adj, w = read_kep(filename)
        self._adj = adj
        self._cycles = get_all_cycles(adj, length)
        self._instance_updated = False

    ###
    # HANDLE GUROBI MODEL
    ###
    def create_model(self, verbose=0):
        self._model = Model(self.name if self.name else "KPD instance")
        self._model.Params.OutputFlag = verbose
        self._x={}
        self._cmap, self._nmap, self._on_cyc = {}, {}, {}
        for i in self._adj:
            self._on_cyc[i] = set()
        n = 1
        for c in self._cycles:
            self._cmap[c] = n
            self._nmap[n] = c
            for i in c:
                self._on_cyc[i].add(n)
            self._x[n] = self._model.addVar(vtype="B", name="x(%s)"%(n))
            n += 1
        self._model.update()
        print("created variables", time.clock(), "s")

        # Ensure that each pair only appears in one cycle for each solution
        for i in self._on_cyc:
            cs = self._on_cyc[i]
            if len(cs) > 1:
                self._model.addConstr(quicksum(self._x[j] for j in cs) <= 1, "cycles(%s)" % i)
        print("added constraints", time.clock(), "s")
        self._instance_updated = True
        self._opt_called = False

    def reset_model(self):
        self.create_model(self._model.Params.OutputFlag)

    ###
    # RESTRICTIONS AND OBJECTIVE DEFINITION AND SOLVE
    ###

    def maximize(self, func):

        self._opt_restrictions.append((func, GRB.MAXIMIZE, None))

    def minimize(self, func):

        self._opt_restrictions.append((func, GRB.MINIMIZE, None))

    def sort_lex(self, func, ascending=False, maximize=True):

        maxmin = GRB.MAXIMIZE if maximize else GRB.MINIMIZE
        self._opt_restrictions.append((func, maxmin, ascending))

    def solve(self):

        first_run = True
        n_criteria = len(self._opt_restrictions)
        for (i,(func, opt_dir, ascending)) in enumerate(self._opt_restrictions):
            if not first_run:
                self._model.addConstr(self._model.getObjective() >= self._model.PoolObjVal, f"CritConstr")

            if ascending is None:
                coefs = {}
                for c in self._cycles:
                    coefs[c] = func(c)
                self._model.setObjective(quicksum(coefs[c] * self._x[self._cmap[c]] for c in self._cycles), opt_dir)
                self._model.Params.PoolSearchMode = 2  # set search mode for finding multiple solutions
                self._model.Params.PoolGap = 0  # find only solutions 0% worse that optimum
                self._model.Params.PoolSolutions = 1 if i < n_criteria-1 else 1000
                self._model.optimize()
                first_run=False
            else:
                func_map_cycles = [*map(func, [c for c in self._cycles])]
                all_vals = []
                for c in func_map_cycles:
                    all_vals.extend(c)
                # map(all_vals.extend, func_map_cycles)
                all_vals = sorted(list(set(all_vals)), reverse=not ascending)
                opt_func = self.maximize if opt_dir == GRB.MAXIMIZE else self.minimize
                for val in all_vals:
                    coefs = {}
                    filter_c=lambda c: len([*filter(lambda x: x == val,func(c))])
                    for c in self._cycles:
                        coefs[c] = filter_c(c)
                    self._model.setObjective(quicksum(coefs[c] * self._x[self._cmap[c]] for c in self._cycles), opt_dir)
                    self._model.Params.PoolSearchMode = 2  # set search mode for finding multiple solutions
                    self._model.Params.PoolSolutions = 1000  # if i < len(all_toe)-1 else 1000  # !!!!! set the number of different solutions desired
                    self._model.Params.PoolGap = 0  # find only solutions 0% worse that optimum
                    self._model.Params.PoolSolutions = 1 if i < n_criteria - 1 else 1000
                    self._model.optimize()
                    self._model.addConstr(self._model.getObjective() >= self._model.PoolObjVal, f"CritConstr")
        self._get_solutions()

    ###
    # GET SOLUTIONS
    ###

    def _get_solutions(self):
        EPS = 1.e-4
        all_sols = set()
        for i in range(self._model.SolCount):
            self._model.Params.SolutionNumber = i
            print(i, "objective:", self._model.PoolObjVal, end="\t")
            sol = []
            for j in self._x:
                if self._x[j].Xn > EPS:
                    sol.append(self._nmap[j])
            sol.sort()
            print(sol)
            sol = frozenset(sol)
            assert sol not in all_sols
            all_sols.add(sol)
        return all_sols

    def show_solutions(self):
        if not self.solutions:
            print("No solutions available. Perhaps model was not yet solved.")
        else:
            for i, sol in enumerate(self.solutions):
                print(i, list(sol))