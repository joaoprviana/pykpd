from kpd_opt import *


if __name__ == '__main__':
    kpd = KPD("Example problem")
    kpd.load_pair_data("char/characterization.csv")
    kpd.load_cycles("enumeraodeciclos/in.input.gz", 3)
    kpd.create_model()
    print()

criteria = 2

# 1. solutions which serve the maximum the number of patients waiting longer, lexicographically
if criteria == 1:
    kpd.sort_lex(lambda cycle: [*map(lambda pair: kpd._db[pair].patient.time_of_entry, cycle)])

# 2. solutions with maximum number of transplants which serve the maximum the number of patients waiting longer,
    # lexicographically
if criteria == 2:
    kpd.maximize(len)
    # kpd.solve()
    kpd.sort_lex(lambda cycle: [*map(lambda pair: kpd._db[pair].patient.time_of_entry, cycle)])
    kpd.solve()
    kpd.show_solutions()

# 3. solutions that minimize the difference of age between donor and patient, summed for all patients,
# with maximum transplants
if criteria == 3:
    def get_sum_age_difference(cycle):
        len_cycle = len(cycle)
        result_cycle = []
        for i in range(len_cycle):
            age_difference = abs(kpd._db[cycle[i % len_cycle]].donor.age - kpd._db[cycle[(i + 1) % len_cycle]].patient.age)
            result_cycle.append(age_difference)
        return sum(result_cycle)

    kpd.maximize(len)
    # kpd.solve()
    kpd.minimize(get_sum_age_difference)
    kpd.solve()
    kpd.show_solutions()

# 4. solutions that minimize the maximum difference of age between donor and patient, with maximum transplants
#     crit4 = results.get_age_difference().flatten().max().minimize()

# 5. solutions with maximum number of transplants and minimum number of international transplants
#
# 6. solutions with maximum number of transplants without any international transplants
#
# 7. solutions that minimize PRA, summed for all patients
if criteria == 7:
    def get_sum_pra(cycle):
        pra_getter = lambda pair: kpd._db[pair].patient.PRA
        return sum([*map(pra_getter, cycle)])

    kpd.maximize(get_sum_pra)
    kpd.solve()
    kpd.show_solutions()

# 8. solutions with maximum transplants that minimize PRA, summed for all patients
if criteria == 8:
    def get_sum_pra(cycle):
        pra_getter = lambda pair: kpd._db[pair].patient.PRA
        return sum([*map(pra_getter, cycle)])

    kpd.maximize(len)
    kpd.solve()
    kpd.maximize(get_sum_pra)
    kpd.solve()
    kpd.show_solutions()

# 9. solutions with maximum transplants that minimize the number of patients transplanted with PRA above 80
#     filter_above_80 = lambda x: x > 0.80
if criteria == 9:
    def get_num_after_filter(c):
        return len([*filter(lambda pair: kpd._db[pair].patient.PRA > 0.80, c)])

    kpd.maximize(len)
    kpd.solve()
    kpd.maximize(get_num_after_filter)
    kpd.solve()
    kpd.show_solutions()

# 10. solutions with maximum transplants that minimize the distance between donor and patient, summed for all patients
#
# 11. solutions with maximum transplants that minimize the maximum distance between a donor and a patient
#
# 12. solutions where Barack Obama is served, with maximum number of transplants
#
# 13. solutions with maximum transplants that minimize the exchanges between Antarctica and Greenland
#
# 14. solutions with maximum transplants that maximizes number of O-type patients transplanted.
if criteria == 14:
    def get_num_blood_type(c,bt):
        result = len([*filter(lambda pair: kpd._db[pair].patient.ABO == bt,c)])
        return result

    kpd.maximize(len)
    kpd.solve()
    kpd.maximize(lambda c: get_num_blood_type(c,'O'))
    kpd.solve()
    kpd.show_solutions()
# 15. solutions where maximum number of O-type patients are transplanted followed by maximization of number of transplants.
if criteria == 15:
    def get_num_blood_type(c,bt):
        result = len([*filter(lambda pair: kpd._db[pair].patient.ABO == bt,c)])
        return result

    kpd.maximize(lambda c: get_num_blood_type(c,'O'))
    kpd.solve()
    kpd.maximize(len)
    kpd.solve()
    kpd.show_solutions()
    #NA
# 16. solutions that maximize the number of transplants using the minimum number of altruistic donors

print('Finished!')