from copy import deepcopy
from operator import itemgetter
import numpy as np

criteria_to_func = {
    'ABO': 'is_blood_match',
    'HLA': 'is_hla_comp'
}
PATIENT = 1
DONOR = 0

# _verb_list = [
#     "applies",
#     "gets",
#     "performs"
# ]

_phrase_completion_list = [
    "given values",
    "set of extracted features"
]
_description_templates = [
    "applies {funcname} to {completion}",
    "performs {funcname} on {completion}"
]

_description_templates =[
    "{funcname} list of values.",
    "{funcname} set of extracted features.",
    "apply {funcname} to {completion}.",
    "perform {funcname} on {completion}.",
]

class Patient():

    def __init__(self, ABO='', AB_HLA_A=[], AB_HLA_B=[], AB_HLA_DQ=[], AB_HLA_DR=[], age=None, time_of_entry=None, PRA=None):
        self.ABO = ABO
        self.AB_HLA_A = AB_HLA_A
        self.AB_HLA_B = AB_HLA_B
        self.AB_HLA_DQ = AB_HLA_DQ
        self.AB_HLA_DR = AB_HLA_DR
        self.age = age
        self.time_of_entry = time_of_entry
        self.PRA = PRA



class Donor():

    def __init__(self, ABO='', HLA_A=[], HLA_B=[], HLA_DQ=[], HLA_DR=[], age=None):
        self.ABO = ABO
        self.HLA_A = HLA_A
        self.HLA_B = HLA_B
        self.HLA_DQ = HLA_DQ
        self.HLA_DR = HLA_DR
        self.age = age

class Pair:

    def __init__(self, donor, patient, id=None):
        self.donor = donor
        self.patient = patient
        self.id = id

    def __str__(self):
        if self.id is not None:
            return "Pair {:>3}".format(self.id)
        else:
            return "Pair #ID#"

    def __repr__(self):
        return self.__str__()

class KPD():

    def __init__(self, pairs=None, patients=None, donors=None):
        if pairs:
            patients = list(map(itemgetter(0), pairs))
            donors = list(map(itemgetter(1), pairs))
        self.patients = patients
        self.donors = donors
        if not patients and not donors:
            print('KPD initialized with no participants.')
        elif not patients and donors:
            print('KPD initialized with no patients.')
        elif patients and not donors:
            print('KPD initialized with no donors.')

        self.criteria = ['ABO', 'HLA']

    @staticmethod
    def is_blood_match(pat, don):
        if pat.ABO == 'AB':
            return True
        elif don.ABO == 'O':
            return True
        else:
            return pat.ABO == don.ABO

    @staticmethod
    def is_hla_comp(pat, don):
        all_comps = [not (pat.AB_HLA_A and don.HLA_A)]
        all_comps.append(not (pat.AB_HLA_B and don.HLA_B))
        all_comps.append(not (pat.AB_HLA_DQ and don.HLA_DQ))
        all_comps.append(not (pat.AB_HLA_DR and don.HLA_DR))
        return all(all_comps)

    def check_comp(self, pat, don):
        if self.criteria == []:
            print('Tried to compare a patient/donor pair without any established criteria.')
            return True
        else:
            partial_comp = list(map(lambda x: self.__getattribute__(criteria_to_func[x])(pat, don), self.criteria))
            return all(partial_comp)

#
# HELPER FUNCTIONS
#

class Criteria:
    def __init__(self, criteria, description="Criteria: No description."):
        self._criteria = criteria
        self._description = description

    def __call__(self, sol):
        return self._criteria(sol)

    def add_description(self, description=None):
        if description!="" or description!=None:
            self._description = str(description.strip())
        else:
            self._description = "Criteria: No description."

    def __str__(self):
        return self._description

    def __repr__(self):
        return self.__str__()

def _flatten(sol):
    return sum(sol, [])


def _get_time_of_entry(sol):
    toe_getter = lambda pair: pair.patient.time_of_entry
    cycle_map = lambda cycle: [*map(toe_getter, cycle)]
    return [*map(cycle_map, sol)]

def _get_pra(sol):
    pra_getter = lambda pair: pair.PRA
    cycle_map = lambda cycle: [*map(pra_getter, cycle)]
    return [*map(cycle_map, sol)]

def filter_blood_type(bt):
    def bt_getter(sol):
        is_bt = lambda x: x.patient.ABO == bt
        sol = [*filter(is_bt, sol)]
        return sol

    return bt_getter


def sort_ascending(sol):
    return sorted(sol, reverse=False)


def sort_descending(sol):
    return sorted(sol, reverse=True)


def maximize(sol):
    return Down(sol)


def minimize(sol):
    return sol


def sort_lex_descending(sol):
    return Down(sort_descending(sol))


class Alternative:

    def __init__(self, sol_dict, description=''):
        self._alt_map = deepcopy(sol_dict)
        self._description = description

    def add_description(self, description):
        if self.get_description():
            old_description = self.get_description()
            old_description = old_description.strip('.') + ', '
            old_description = old_description.capitalize()

            description = description.strip()
            description = (description[0], description[1:])
            description = description[0].lower() + description[1]
            self._description = old_description + description
        else:
            self._description = description

    def get_description(self):
        return self._description

    def map(self, f, description=''):
        new_dict = dict((k, f(v)) for k, v in self._alt_map.items())
        result_alt = Alternative(new_dict, self.get_description())
        if description:
            result_alt.add_description(description)
        else:
            funcname = process_func_name(f.__name__)
            if len(funcname.split(' ')) > 1:
                description = funcname+'.'
            else:
                description = np.random.choice(_description_templates).capitalize()
                completion = np.random.choice(_phrase_completion_list)
                description = description.format(funcname=funcname, completion=completion)
            result_alt.add_description(description)
        return result_alt

    @classmethod
    def combine(cls, *alts):
        final_keys = set()
        for alt in alts:
            assert isinstance(alt, Alternative), "Given inputs should be a mapping of alternatives."
            if final_keys == set():
                final_keys = alt._alt_map.keys()
            else:
                final_keys = final_keys & alt._alt_map.keys()
        combine_dict = dict((k, tuple([*map(lambda alternative: alternative._alt_map[k], alts)])) for k in final_keys)
        return Alternative(combine_dict)

    def get_age_difference(self):
        return self.map(_get_age_difference)#, 'Get the age difference between each matched patient and donor.')

    def get_time_of_entry(self):
        return self.map(_get_time_of_entry)

    def get_pra(self):
        return self.map(_get_pra)

    def flatten(self):
        return self.map(_flatten)#, 'Flatten group of values into a single list.')

    def sum(self):
        return self.map(sum, 'Sum all values in list.')

    def filter_blood_type(self, bt):
        return self.map(filter_blood_type(bt), 'Filter the list of pairs to get only patients with type-O blood.')

    def sort_ascending(self):
        return self.map(sort_descending, 'Sort values in ascending order.')

    def sort_descending(self):
        return self.map(sort_descending, 'Sort values in descending order.')

    def maximize(self):
        return self.map(maximize)#, 'Set objective to maximize value in criteria.')

    def minimize(self):
        return self.map(minimize)#, 'Set objective to maximize value in criteria.')

    def sort_lex_descending(self):
        return self.map(sort_lex_descending)#, 'Sort in descending, lexicographical order.')

    def len(self):
        return self.map(len)#, 'Compute the length of list of values.')

    def max(self):
        return self.map(max)

    def min(self):
        return self.map(min)

class Down:

    def __init__(self, val):
        self._container = val

    def get(self):
        return self._container

    def __lt__(self, other):
        if not isinstance(other,Down):
            error_msg = "Operation not supported between instances of type '{type1}' and '{type2}'."
            error_msg = error_msg.format(type1=type(self), type2=type(other))
            raise TypeError(error_msg)
        else:
            return self._container > other._container

    def __repr__(self):
        return "Descending({})".format(self._container)

def process_func_name(name):
    name = name.strip('_')
    name = name.split('_')
    name = ' '.join(name).capitalize()
    return name